use clap::{App, Arg};
use serde::{Deserialize, Serialize};
use tokio::{process, signal, spawn, sync::watch, sync::OnceCell, time};
use tracing::{error, info};

static INPUT_FILENAME: OnceCell<String> = OnceCell::const_new();
static DB: OnceCell<sled::Db> = OnceCell::const_new();
static TIMELIMIT: OnceCell<u64> = OnceCell::const_new();
static RAND_PARAMS: OnceCell<bool> = OnceCell::const_new();

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = App::new("aml-ec-digger")
        .arg(
            Arg::with_name("input")
                .short("i")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("time")
                .short("t")
                .takes_value(true)
                .required(true),
        )
        .arg(Arg::with_name("rand_params").short("p"))
        .get_matches();
    let input_value = matches.value_of("input").unwrap();
    INPUT_FILENAME.set(input_value.to_string())?;
    let time_value = matches.value_of("time").unwrap();
    TIMELIMIT.set(time_value.parse::<u64>()?)?;
    RAND_PARAMS.set(matches.is_present("rand_params"))?;

    // Get filename part of the input file path
    let input_basename = std::path::Path::new(&INPUT_FILENAME.get().unwrap())
        .file_name()
        .unwrap()
        .to_str()
        .unwrap()
        .to_string();

    // Init logger
    tokio::fs::create_dir_all("./dig-log").await?;
    let file_appender =
        tracing_appender::rolling::hourly("./dig-log/", format!("{}.log", input_basename));
    let (non_blocking, _guard) = tracing_appender::non_blocking(file_appender);
    tracing_subscriber::fmt().with_writer(non_blocking).init();

    // Init DB
    let tree = sled::open(format!("{}.sled", input_basename))?;
    DB.set(tree)?;

    // Channel to notify tasks about CtrlC
    let (end_tx, end_rx) = watch::channel(false);

    // The main tasks
    let ncpus = num_cpus::get_physical();
    let tasks: Vec<_> = (0..ncpus)
        .map(|task_num| {
            let end_rx = end_rx.clone();
            spawn(async move {
                if let Err(e) = start_digger(end_rx).await {
                    error!("Task {} encountered error:\n{:?}\n", task_num, e);
                }
            })
        })
        .collect();

    // Listen on CtrlC
    spawn(async move {
        let _ = signal::ctrl_c().await;
        info!("CtrlC signal fired");
        end_tx.send(true).expect("Failed to send to end_tx");
    });
    futures::future::join_all(tasks).await;

    let db_iter = DB.get().unwrap().iter();
    let mut candidates: Vec<_> = db_iter
        .filter_map(|res| res.ok())
        .map(|(_key, value)| {
            let response: AmlEcResponse = serde_cbor::from_slice(&value).unwrap();
            response
        })
        .collect();
    candidates.sort_by_key(|res| res.solve_ms);

    let top_ten: Vec<_> = candidates.iter().take(10).collect();
    info!("Top ten:{:#?}", top_ten);

    Ok(())
}

fn ec_args() -> Vec<String> {
    let mut args = vec![
        INPUT_FILENAME.get().unwrap().clone(),
        "/dev/null".to_string(),
        "-j".to_string(),
        "-r".to_string(),
    ];
    if *RAND_PARAMS.get().unwrap() {
	args.push("-p".to_string());
    }
    args
}

async fn start_digger(end_rx: watch::Receiver<bool>) -> Result<(), Box<dyn std::error::Error>> {
    let timelimit = *TIMELIMIT.get().unwrap();
    while !*end_rx.borrow() {
        let mut child = process::Command::new("./ec")
            .stdout(std::process::Stdio::piped())
            .stderr(std::process::Stdio::inherit())
            .args(ec_args())
            .spawn()?;

        match time::timeout(time::Duration::from_secs(timelimit), child.wait()).await {
            Ok(Ok(status)) if status.success() => {
                let output = child.wait_with_output().await?;
                info!("Got stdout: {}", String::from_utf8(output.stdout.clone())?);
		let mut stdout = output.stdout.clone();
		remove_leading_c(&mut stdout);
                let response: AmlEcResponse = serde_json::from_slice(&stdout)?;

                info!("Got good result: {:#?}", response);

                let serialized_response = serde_cbor::to_vec(&response)?;
                let id = DB.get().unwrap().generate_id()?;

                DB.get()
                    .unwrap()
                    .insert(id.to_be_bytes(), serialized_response)?;

                info!("Written to database");
            }
            Ok(Ok(status)) => {
                info!("Bad exit status: {:?}", status);
            }
            Ok(Err(e)) => {
                info!("Wait failed: {:?}", e);
            }
            Err(_) => {
                info!("Timeout; killing");
                child.kill().await?;
            }
        }
    }

    Ok(())
}

#[derive(Debug, Serialize, Deserialize)]
struct AmlEcResponse {
    params: AmlEcParams,
    parse_ms: u32,
    solve_ms: u32,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize)]
struct AmlEcParams {
    K: f64,
    R: f64,
    sizeLBDQueue: f64,
    sizeTrailQueue: f64,
    firstReduceDB: i32,
    incReduceDB: i32,
    specialIncReduceDB: i32,
    lbLBDFrozenClause: u32,
    coLBDBound: i32,
    lbSizeMinimizingClause: i32,
    lbLBDMinimizingClause: u32,
    var_decay: f64,
    max_var_decay: f64,
    clause_decay: f64,
    random_var_freq: f64,
    random_seed: f64,
    ccmin_mode: i32,
    phase_saving: i32,
    rnd_init_act: bool,
    garbage_frac: f64,
}

fn remove_leading_c(v: &mut Vec<u8>) {
    if let Some(99) = v.first() {
	v.drain(0..1);
    }
}
